﻿

#include <iostream>
#include <math.h>

class Homework
{
private:
    int a;

public:

    int GetA()
    {
        return a;
    }

    void SetA(int newA)
    {
        a = newA;
    }

};

class Vector
{
public:
    Vector() : x(0), y(0), z(0)
    {}
    Vector (double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        std::cout << '\n' << x << ' ' << y << ' ' << z << "\n";
    }

    

    void lv()
    {
        double lv = sqrt( pow(x, 2) + pow(y, 2) + pow(z, 2) );
        std::cout << lv;
    }

private:
    double x;
    double y;
    double z;
};

int main()
{
    Homework abc;
    abc.SetA(17);
    std::cout << abc.GetA();

    Vector v(25, 25, 25);
    v.Show();
    v.lv();

    
}

